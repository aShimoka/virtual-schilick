﻿using UnityEngine;
using Unity.Entities;
using UnityEngine.UI;
using UnityEditor;
using UnityEngine.SceneManagement;


/// <summary>
/// System used to handle the quit buttons in the UI of the app.
/// </summary>
public class QuitButtonSystem : ComponentSystem {
    /// <summary>
    /// List of all the components used by the system.
    /// </summary>
    struct Components {
        public Button Button;
        public QuitButton TransitionButton;
    }

    /// <summary>
    /// Called by the engine on the beginning of the game.
    /// </summary>
    protected override void OnStartRunning() {
        // Check if the scene was updated.
        SceneManager.activeSceneChanged += OnLevelChange;

        // Call the method once on the start of the app.
        OnLevelChange(new Scene(), new Scene());
    }

    /// <summary>
    /// Event triggered when the scene changes.
    /// </summary>
    /// <param name="oldScene">Old scene.</param>
    /// <param name="newScene">New scene.</param>
    private void OnLevelChange(Scene oldScene, Scene newScene) {
        // Loop through all of the entities of the World.
        foreach (var entity in GetEntities<Components>()) {
            // Add a new listener to the button that calls the GotoScene method.
            entity.Button.onClick.AddListener(delegate {
                QuitApplication();
            });
        }

    }

    /// <summary>
    /// Closes the application or stops the editor play mode.
    /// </summary>
    private void QuitApplication() {
        // Check if the editor is running.
        #if UNITY_EDITOR
        EditorApplication.Beep();
        EditorApplication.isPlaying = false;
        #else
        Application.Quit();
        #endif
    }

    protected override void OnUpdate() {}
}
