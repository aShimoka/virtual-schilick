﻿using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Button used to transit between multiple scenes within the app.
/// Used by the <see cref="TransitionButtonSystem"/> to handle transitions.
/// </summary>
public class TransitionButton : MonoBehaviour
{
    /// <summary>
    /// The name of the scene to transition into.
    /// </summary>
    public string SceneName;
}