using UnityEngine;

/// <summary>
/// Button used to quit the app, or the editor playing mode.
/// </summary>
public class QuitButton : MonoBehaviour
{}